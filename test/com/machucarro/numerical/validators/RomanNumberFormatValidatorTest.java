package com.machucarro.numerical.validators;

import com.machucarro.numerical.exceptions.RomanNumberFormatException;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by machucarro on 19/03/14.
 */
public class RomanNumberFormatValidatorTest {

    @Test
    public void testValidate() throws Exception {
        RomanNumberFormatValidator validator = new RomanNumberFormatValidator();
        String[] validRomans = { "CMXXXIX", "MCDXLIV", "MMM", "MMMDCCC", "DCCC", "LXXX", "VIII",
                "MMMDCCCLXXXVIII", "CDXC" };
        for (String validRoman : validRomans) {
            assertTrue(validRoman
                    + " should be a valid roman numeric Expression but was recognized as invalid",
                    validator.validate(validRoman));
        }
        String[] invalidRomans = { "LMXXXIX", "MCDDXLIV", "MMMM", "DMMMDCCC", "DCDDCDC", "LIXXX",
                "IVIII", "MMMMDCCCLXXXVIII", "CDC", null, "" };
        for (String invalidRoman : invalidRomans) {
            try {
                validator.validate(invalidRoman);
                fail(invalidRoman
                        + " should be a non valid roman numeric Expression but was recognized as valid. A "
                        + "RomanNumberFormatException should be thrown");
            } catch (RomanNumberFormatException e) {
                assertNotNull(
                        invalidRoman
                                + " should be a non valid roman numeric Expression but was recognized as valid",
                        e);
            } catch (Exception e) {
                fail("Validation of " + invalidRoman
                        + " should have caused a RomanNumberFormatException but other "
                        + "Exception was thrown");
            }
        }
    }

    @Test
    public void testValidateForConversion() throws Exception {
        IntegerToRomanConversionValidator validator = new IntegerToRomanConversionValidator();
        Integer[] validArabics = { 50, 1, 3888 };
        for (Integer validArabic : validArabics) {
            assertTrue(
                    validArabic
                            + " should be a suitable arabic numeric Expression for conversion to roman but "
                            + "was recognized as invalid", validator.validate(validArabic));
        }
        Integer[] invalidArabics = { -1, 0, null, 3889, 10000 };
        for (Integer invalidArabic : invalidArabics) {
            try {
                validator.validate(invalidArabic);
                fail(invalidArabic
                        + " should be a non convertible arabic numeric Expression but was recognized"
                        + " as valid for conversion. A RomanNumberFormatException should be thrown");
            } catch (RomanNumberFormatException e) {
                assertNotNull(
                        invalidArabic
                                + " should be a non convertible arabic numeric Expression but was recognized as valid",
                        e);
            } catch (Exception e) {
                fail("Validation for conversion of " + invalidArabic + " should have caused a "
                        + "RomanNumberFormatException but other Exception was thrown");
            }
        }
    }
}
