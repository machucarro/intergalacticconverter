package com.machucarro.numerical.validators;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.machucarro.numerical.exceptions.IntergalacticNumberFormatException;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.numbers.RomanNumber;

/**
 * @author MAchucarro
 */
public class IntergalacticNumberFormatValidatorTest {
    private static final String A = "aaaa";
    private static final String B = "bbbb";
    private static final String C = "cccc";
    private static final String D = "dddd";
    private static final String E = "eeee";
    private static final String F = "ffff";
    private static final String G = "8888";
    private static final String NON_MAPPED_DIGIT = "kjbdkjb";
    private static final String DS = IntergalacticNumberFormatValidator.DIGIT_SEPARATOR;
    private static final String INVALID_DIGIT = "jjnn&?";

    @Before
    public void setUp() throws Exception {
        IntergalacticNumber.addDigit(A, RomanNumber.I);
        IntergalacticNumber.addDigit(B, RomanNumber.V);
        IntergalacticNumber.addDigit(C, RomanNumber.X);
        IntergalacticNumber.addDigit(D, RomanNumber.L);
        IntergalacticNumber.addDigit(E, RomanNumber.C);
        IntergalacticNumber.addDigit(F, RomanNumber.D);
        IntergalacticNumber.addDigit(G, RomanNumber.M);
    }

    @After
    public void tearDown() throws Exception {
        IntergalacticNumber.getDIGITS();
        for (IntergalacticNumber intergalacticDigit : IntergalacticNumber.getDIGITS()) {
            IntergalacticNumber.removeDigit(intergalacticDigit);
        }
    }

    @Test
    public void testValidate() throws Exception {
        IntergalacticNumberFormatValidator validator = new IntergalacticNumberFormatValidator();
        String[] validIntergalactics = {
                E + DS + G + DS + C + DS + C + DS + C + DS + A + DS + C,
                G + DS + E + DS + F + DS + C + DS + D + DS + A + DS + B,
                G + DS + G + DS + G,
                G + DS + G + DS + G + DS + F + DS + E + DS + E + DS + E,
                B + DS + A + DS + A + DS + A,
                G + DS + G + DS + G + DS + F + DS + E + DS + E + DS + E + DS + D + DS + C + DS + C
                        + DS + C + DS + B + DS + A + DS + A + DS + A, E + DS + F + DS + C + DS + E };
        for (String validIntergalactic : validIntergalactics) {
            assertTrue(validIntergalactic
                    + " should be a valid intergalactic numeric Expression but was recognized "
                    + "as invalid", validator.validate(validIntergalactic));
        }
        String[] invalidIntergalactics = {
                D + DS + G + DS + C + DS + C + DS + C + DS + A + DS + C,
                G + DS + E + DS + F + DS + F + DS + C + DS + D + DS + A + DS + B,
                G + DS + G + DS + G + DS + G,
                F + DS + G + DS + G + DS + G + DS + F + DS + E + DS + E + DS + E,
                F + DS + E + DS + F + DS + F + DS + E + DS + F + DS + E,
                D + DS + A + DS + C + DS + C + DS + C,
                A + DS + B + DS + A + DS + A + DS + A,
                G + DS + G + DS + G + DS + G + DS + F + DS + E + DS + E + DS + E + DS + D + DS + C
                        + DS + C + DS + C + DS + B + DS + A + DS + A + DS + A, E + DS + F + DS + E,
                NON_MAPPED_DIGIT, INVALID_DIGIT, null, "" };
        for (String invalidIntergalactic : invalidIntergalactics) {
            try {
                validator.validate(invalidIntergalactic);
                fail(invalidIntergalactic
                        + " should be a non valid intergalactic numeric Expression but was recognized as valid. A "
                        + "IntergalacticNumberFormatException should be thrown");
            } catch (IntergalacticNumberFormatException e) {
                assertNotNull(
                        invalidIntergalactic
                                + " should be a non valid intergalactic numeric Expression but was recognized as valid",
                        e);
            } catch (Exception e) {
                fail("Validation of " + invalidIntergalactic
                        + " should have caused a IntergalacticNumberFormatException but other "
                        + "Exception was thrown");
            }
        }
    }
}
