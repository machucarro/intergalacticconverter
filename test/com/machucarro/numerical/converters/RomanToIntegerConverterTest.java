package com.machucarro.numerical.converters;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author MAchucarro
 */
public class RomanToIntegerConverterTest {
    private Map<String, Integer> testValidRomanCases = new HashMap<String, Integer>();

    @Before
    public void setUp() throws Exception {
        testValidRomanCases.put("I", 1);
        testValidRomanCases.put("V", 5);
        testValidRomanCases.put("X", 10);
        testValidRomanCases.put("L", 50);
        testValidRomanCases.put("C", 100);
        testValidRomanCases.put("D", 500);
        testValidRomanCases.put("M", 1000);
        testValidRomanCases.put("II", 2);
        testValidRomanCases.put("III", 3);
        testValidRomanCases.put("IV", 4);
        testValidRomanCases.put("VI", 6);
        testValidRomanCases.put("VII", 7);
        testValidRomanCases.put("VIII", 8);
        testValidRomanCases.put("IX", 9);
        testValidRomanCases.put("XX", 20);
        testValidRomanCases.put("XLII", 42);
        testValidRomanCases.put("CMXXXIX", 939);
        testValidRomanCases.put("MCDXLIV", 1444);
        testValidRomanCases.put("MMMDCCCLXXXVIII", 3888);
        testValidRomanCases.put("CDXC", 490);

    }

    @Test
    public void testConvert() {
        // Test for valid Roman inputs
        for (Map.Entry<String, Integer> validCase : testValidRomanCases.entrySet()) {
            Assert.assertEquals("Value for " + validCase.getKey() + " not properly calculated",
                    validCase.getValue(), RomanToIntegerConverter.convert(validCase.getKey()));
        }

    }
}
