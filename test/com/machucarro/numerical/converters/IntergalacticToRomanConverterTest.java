package com.machucarro.numerical.converters;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.numbers.RomanNumber;
import com.machucarro.numerical.validators.IntergalacticNumberFormatValidator;

/**
 * @author MAchucarro
 */
public class IntergalacticToRomanConverterTest {

    private static final String A = "aaaa";
    private static final String B = "bbbb";
    private static final String C = "cccc";
    private static final String D = "dddd";
    private static final String E = "eeee";
    private static final String F = "ffff";
    private static final String G = "gggg";
    private static final String DS = IntergalacticNumberFormatValidator.DIGIT_SEPARATOR;

    private Map<String, RomanNumber> validCases = new HashMap<String, RomanNumber>();

    @Before
    public void setUp() throws Exception {
        IntergalacticNumber.addDigit(A, RomanNumber.I);
        IntergalacticNumber.addDigit(B, RomanNumber.V);
        IntergalacticNumber.addDigit(C, RomanNumber.X);
        IntergalacticNumber.addDigit(D, RomanNumber.L);
        IntergalacticNumber.addDigit(E, RomanNumber.C);
        IntergalacticNumber.addDigit(F, RomanNumber.D);
        IntergalacticNumber.addDigit(G, RomanNumber.M);

        validCases.put(A, RomanNumber.I);
        validCases.put(B, RomanNumber.V);
        validCases.put(C, RomanNumber.X);
        validCases.put(D, RomanNumber.L);
        validCases.put(E, RomanNumber.C);
        validCases.put(F, RomanNumber.D);
        validCases.put(G, RomanNumber.M);
        validCases.put(A + DS + A, new RomanNumber("II"));
        validCases.put(A + DS + A + DS + A, new RomanNumber("III"));
        validCases.put(A + DS + B, new RomanNumber("IV"));
        validCases.put(B + DS + A, new RomanNumber("VI"));
        validCases.put(B + DS + A + DS + A, new RomanNumber("VII"));
        validCases.put(B + DS + A + DS + A + DS + A, new RomanNumber("VIII"));
        validCases.put(A + DS + C, new RomanNumber("IX"));
        validCases.put(C + DS + C, new RomanNumber("XX"));
        validCases.put(C + DS + D + DS + A + DS + A, new RomanNumber("XLII"));
        validCases.put(E + DS + G + DS + C + DS + C + DS + C + DS + A + DS + C, new RomanNumber(
                "CMXXXIX"));
        validCases.put(G + DS + E + DS + F + DS + C + DS + D + DS + A + DS + B, new RomanNumber(
                "MCDXLIV"));
        validCases.put(G + DS + G + DS + G + DS + F + DS + E + DS + E + DS + E + DS + D + DS + C
                + DS + C + DS + C + DS + B + DS + A + DS + A + DS + A, new RomanNumber(
                "MMMDCCCLXXXVIII"));
        validCases.put(E + DS + F + DS + C + DS + E, new RomanNumber("CDXC"));
    }

    @After
    public void tearDown() throws Exception {
        IntergalacticNumber.getDIGITS();
        for (IntergalacticNumber intergalacticDigit : IntergalacticNumber.getDIGITS()) {
            IntergalacticNumber.removeDigit(intergalacticDigit);
        }
        validCases.clear();
    }

    @Test
    public void testConvert() throws Exception {
        // Test for valid Intergalactic inputs
        for (Map.Entry<String, RomanNumber> validCase : validCases.entrySet()) {
            Assert.assertEquals("Value for " + validCase.getKey() + " not properly calculated",
                    validCase.getValue(), IntergalacticToRomanConverter.convert(validCase.getKey()));
        }
    }
}
