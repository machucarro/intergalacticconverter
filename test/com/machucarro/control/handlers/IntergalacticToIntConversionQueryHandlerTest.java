package com.machucarro.control.handlers;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.machucarro.control.exceptions.ExecutionTerminationRequest;
import com.machucarro.control.exceptions.QueryProcessingException;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.numbers.RomanNumber;

/**
 * @author MAchucarro
 */
public class IntergalacticToIntConversionQueryHandlerTest {
    private static final String TEST_QUERY = "how much is pish tegj glob glob ?";
    private QueryHandler handler = new IntergalacticToIntConversionQueryHandler();

    @Before
    public void setUp() throws Exception {
        IntergalacticNumber.addDigit("glob", RomanNumber.I);
        IntergalacticNumber.addDigit("prok", RomanNumber.V);
        IntergalacticNumber.addDigit("pish", RomanNumber.X);
        IntergalacticNumber.addDigit("tegj", RomanNumber.L);
    }

    @After
    public void tearDown() throws Exception {
        IntergalacticNumber.clearDigits();
    }

    @Test
    public void testHandle() throws Exception {
        try {
            Assert.assertEquals("pish tegj glob glob is 42", handler.handle(TEST_QUERY));
        } catch (ExecutionTerminationRequest executionTerminationRequest) {
            Assert.fail();
        } catch (QueryProcessingException e) {
            Assert.fail();
        }
    }

    @Test
    public void testCanHandle() throws Exception {
        Assert.assertTrue(handler.canHandle(TEST_QUERY));
    }
}
