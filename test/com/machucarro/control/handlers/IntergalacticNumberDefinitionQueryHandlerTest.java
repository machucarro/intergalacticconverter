package com.machucarro.control.handlers;

import org.junit.Assert;
import org.junit.Test;

import com.machucarro.control.exceptions.ExecutionTerminationRequest;
import com.machucarro.control.exceptions.QueryProcessingException;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.numbers.RomanNumber;

/**
 * @author MAchucarro
 */
public class IntergalacticNumberDefinitionQueryHandlerTest {
    private static final String GLOB = "glob";
    private static final String TEST_QUERY_I = GLOB + " is " + RomanNumber.I;
    private static final String PROK = "prok";
    private static final String TEST_QUERY_V = PROK + " is " + RomanNumber.V;
    private static final String PISH = "pish";
    private static final String TEST_QUERY_X = PISH + " is " + RomanNumber.X;
    private static final String TEGJ = "tegj";
    private static final String TEST_QUERY_L = TEGJ + " is " + RomanNumber.L;
    private static QueryHandler handler = new IntergalacticNumberDefinitionQueryHandler();

    @Test
    public void testHandle() throws Exception {
        try {
            Assert.assertEquals("", handler.handle(TEST_QUERY_I));
            Assert.assertEquals(RomanNumber.I, new IntergalacticNumber(GLOB).romanValue());
            Assert.assertEquals("", handler.handle(TEST_QUERY_V));
            Assert.assertEquals(RomanNumber.V, new IntergalacticNumber(PROK).romanValue());
            Assert.assertEquals("", handler.handle(TEST_QUERY_X));
            Assert.assertEquals(RomanNumber.X, new IntergalacticNumber(PISH).romanValue());
            Assert.assertEquals("", handler.handle(TEST_QUERY_L));
            Assert.assertEquals(RomanNumber.L, new IntergalacticNumber(TEGJ).romanValue());
        } catch (ExecutionTerminationRequest n) {
            Assert.fail();
        } catch (QueryProcessingException e) {
            Assert.fail();
        } catch (Exception e) {
            Assert.fail();
        }
        IntergalacticNumber.clearDigits();
    }

    @Test
    public void testCanHandle() throws Exception {
        Assert.assertTrue(handler.canHandle(TEST_QUERY_I));
        Assert.assertTrue(handler.canHandle(TEST_QUERY_V));
        Assert.assertTrue(handler.canHandle(TEST_QUERY_X));
        Assert.assertTrue(handler.canHandle(TEST_QUERY_L));
    }
}
