package com.machucarro.control.handlers;

import org.junit.Assert;
import org.junit.Test;

import com.machucarro.control.exceptions.ExecutionTerminationRequest;
import com.machucarro.control.exceptions.QueryProcessingException;

/**
 * @author MAchucarro
 */
public class ExitQueryHandlerTest {
    private static final String TEST_QUERY = "exit";
    private static final String TEST_QUERY_FALSE = "wabadabadooo!";
    private static final String TEST_QUERY_CASE = "EXIT";
    private QueryHandler exitHandler = new ExitQueryHandler();

    @Test
    public void testHandle() throws Exception {
        try {
            exitHandler.handle(TEST_QUERY);
        } catch (ExecutionTerminationRequest executionTerminationRequest) {
            Assert.assertNotNull(executionTerminationRequest);
        } catch (QueryProcessingException e) {
            Assert.fail();
        }
    }

    @Test
    public void testCanHandle() throws Exception {
        Assert.assertTrue(exitHandler.canHandle(TEST_QUERY));
        Assert.assertFalse(exitHandler.canHandle(TEST_QUERY_FALSE));
        Assert.assertFalse(exitHandler.canHandle(TEST_QUERY_CASE));
    }
}
