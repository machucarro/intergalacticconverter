package com.machucarro.control.handlers;

import com.machucarro.control.exceptions.ExecutionTerminationRequest;
import com.machucarro.control.exceptions.QueryProcessingException;
import com.machucarro.financials.ProductPortfolio;
import com.machucarro.financials.currency.Credit;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.numbers.RomanNumber;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * @author MAchucarro
 */
public class PriceRequestQueryHandlerTest {
    private static BigDecimal silverPrice = new BigDecimal(17);
    private static BigDecimal goldPrice = new BigDecimal(14450);
    private static BigDecimal ironPrice = new BigDecimal(195.5);
    private static final String SILVER = "Silver";
    private static final String GOLD = "Gold";
    private static final String IRON = "Iron";

    private ProductPortfolio portfolio = new ProductPortfolio("test", Credit.getInstance());
    private QueryHandler handler = new PriceRequestQueryHandler(portfolio);

    private static final String TEST_QUERY_1 = "how many Credits is glob prok Silver ?";
    private static final String TEST_QUERY_2 = "how many Credits is glob prok Gold ?";
    private static final String TEST_QUERY_3 = "how many Credits is glob prok Iron ?";

    @Before
    public void setUp() throws Exception {
        IntergalacticNumber.addDigit("glob", RomanNumber.I);
        IntergalacticNumber.addDigit("prok", RomanNumber.V);
        IntergalacticNumber.addDigit("pish", RomanNumber.X);
        IntergalacticNumber.addDigit("tegj", RomanNumber.L);
        portfolio.setPriceFor(SILVER, silverPrice);
        portfolio.setPriceFor(GOLD, goldPrice);
        portfolio.setPriceFor(IRON, ironPrice);
    }

    @After
    public void tearDown() throws Exception {
        IntergalacticNumber.clearDigits();
    }

    @Test
    public void testHandle() throws Exception {
        try {
            Assert.assertEquals("glob prok Silver is 68 Credits", handler.handle(TEST_QUERY_1));
            Assert.assertEquals("glob prok Gold is 57800 Credits", handler.handle(TEST_QUERY_2));
            Assert.assertEquals("glob prok Iron is 782 Credits", handler.handle(TEST_QUERY_3));
        } catch (ExecutionTerminationRequest executionTerminationRequest) {
            Assert.fail();
        } catch (QueryProcessingException e) {
            Assert.fail();
        }
    }

    @Test
    public void testCanHandle() throws Exception {
        Assert.assertTrue(handler.canHandle(TEST_QUERY_1));
        Assert.assertTrue(handler.canHandle(TEST_QUERY_2));
        Assert.assertTrue(handler.canHandle(TEST_QUERY_3));
    }
}
