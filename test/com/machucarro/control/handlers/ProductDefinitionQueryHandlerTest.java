package com.machucarro.control.handlers;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.machucarro.control.exceptions.ExecutionTerminationRequest;
import com.machucarro.control.exceptions.QueryProcessingException;
import com.machucarro.financials.ProductPortfolio;
import com.machucarro.financials.currency.Credit;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.numbers.RomanNumber;

/**
 * @author MAchucarro
 */
public class ProductDefinitionQueryHandlerTest {
    private static BigDecimal silverPrice = new BigDecimal(17);
    private static BigDecimal goldPrice = new BigDecimal(14450);
    private static BigDecimal ironPrice = new BigDecimal(195.5);

    private static final String SILVER = "Silver";
    private static final String TWO = "glob glob";
    private static final String TEST_QUERY_1 = TWO + " " + SILVER + " is "
            + silverPrice.multiply(new BigDecimal(2)) + " Credits";

    private static final String GOLD = "Gold";
    private static final String FOUR = "glob prok";
    private static final String TEST_QUERY_2 = FOUR + " " + GOLD + " is "
            + goldPrice.multiply(new BigDecimal(4)) + " Credits";

    private static final String IRON = "Iron";
    private static final String TWENTY = "pish pish";
    private static final String TEST_QUERY_3 = TWENTY + " " + IRON + " is "
            + ironPrice.multiply(new BigDecimal(20)) + " Credits";
    private ProductPortfolio portfolio = new ProductPortfolio("test portfolio",
            Credit.getInstance());
    private QueryHandler handler = new ProductDefinitionQueryHandler(portfolio);

    @Before
    public void setUp() throws Exception {
        IntergalacticNumber.addDigit("glob", RomanNumber.I);
        IntergalacticNumber.addDigit("prok", RomanNumber.V);
        IntergalacticNumber.addDigit("pish", RomanNumber.X);
        IntergalacticNumber.addDigit("tegj", RomanNumber.L);
    }

    @After
    public void tearDown() throws Exception {
        IntergalacticNumber.clearDigits();
    }

    @Test
    public void testHandle() throws Exception {
        try {
            Assert.assertEquals("", handler.handle(TEST_QUERY_1));
            Assert.assertEquals("", handler.handle(TEST_QUERY_2));
            Assert.assertEquals("", handler.handle(TEST_QUERY_3));
            Assert.assertEquals(silverPrice, portfolio.getPriceFor(SILVER, BigDecimal.ONE));
            Assert.assertEquals(goldPrice, portfolio.getPriceFor(GOLD, BigDecimal.ONE));
            Assert.assertEquals(ironPrice, portfolio.getPriceFor(IRON, BigDecimal.ONE));
        } catch (ExecutionTerminationRequest executionTerminationRequest) {
            Assert.fail();
        } catch (QueryProcessingException e) {
            Assert.fail();
        }
    }

    @Test
    public void testCanHandle() throws Exception {
        Assert.assertTrue(handler.canHandle(TEST_QUERY_1));
        Assert.assertTrue(handler.canHandle(TEST_QUERY_2));
        Assert.assertTrue(handler.canHandle(TEST_QUERY_3));
    }
}
