Merchants Guide to the Galaxy

Java Version: JDK7

Installation requisites: Apache Ant, junit-4.11.jar, hamcrest-core-1.3.jar (junit needs it)

Instructions to install:
	- Unzip the file
	- Open the folder "MerchantsGuideToGalaxy"
	- Edit the file "merchantsguidetogalaxy.properties" and set the following variables
			jdk.home.1.7=[path to your jdk7]
			libs.home=[path to junit-4.11.jar and hamcrest-core-1.3.jar]
	- Save it

	- Open a console and navigate to the "MerchantsGuideToGalaxy" folder
	- run the command: ant -f merchantsguidetogalaxy.xml
	- A new file merchants-guide-to-galaxy.jar should be now in the folder

Intructions to Run:
	- for reading the queries from a file
		* Open a console and navigate to the "MerchantsGuideToGalaxy"
		* Run the command: java -jar merchants-guide-to-galaxy.jar < [input-file]
	- for reading the queries from the command line
		* Open a console and navigate to the "MerchantsGuideToGalaxy"
		* Run the command: java -jar merchants-guide-to-galaxy.jar
		* To terminate the program type "exit" and press intro

Libraries used for Testing:
    - junit-4.11.jar
    - hamcrest-core-1.3.jar

JavaDoc: IntergalacticConverter/javadoc/index.html

Brief description and assumptions:

    - I tried to resolve the problem focusing on the reusability of the classes and packages. I tried to make them as
     much independent as possible from the final purpose of the assignment so anyone may reuse them for any
     other purposes. I'm specially proud of the contents of the com.machucarro.control package because I believe the
     pipeline framework I developed for dealing with the user queries is a powerful reusable tool which gives some
     advantages of pipeline frameworks but with a much simpler and lighter implementation.

     - I assumed that the purpose of this Merchant Guide is to carry out financial operations, so that's why I chose
     BigDecimals as calculation format for the amounts which reflect prices. In general I made all the amounts
     convertible to BigDecimal, no mater the format number because I believe it gives the design more flexibility to
     perfor further financial calculations.

     - I also assumed the assignments between numbers can only be in the form of one intergalactic digit (one
     alphanumeric word without spaces) with one roman digit. I considered the possibility of allowing chained
     value assignments in the form asasd is V, gfdi is asasd. But I rejected it because I believe it doesn't bring
     a significant value to the functionality (given the definition of the application) which deserves the design
     changes.

     - It made me smile a lot the woodchuck test case so I took a small license and broke that test case in order to
      implement a special handler for questions which come from the conversation of Guybrush Threepwood in Monkey
      Island 2 abour the woodchuck riddle. I hope you like it and may be smile as well :D

     - All the classes and packages have their proper javadoc documentation which is also included in the zip file
     under the javadoc folder. There you can find a more detailed explanation of the different classes and methods.

     - I did this in my free time during the last week which mostly was at the weekend. It took me whole saturday and
      big part of sunday to make it good enough for my personal quality standard. My arms are painful and I cannot
      feel my ass anymore because of being so long sit so I really hope you like it as much as I liked solving your
      assignment challenge :)

      Thanks and cheers!

      Manuel.

