package com.machucarro;

import com.machucarro.control.exceptions.ExecutionTerminationRequest;
import com.machucarro.control.exceptions.QueryProcessingException;
import com.machucarro.control.handlers.ExitQueryHandler;
import com.machucarro.control.handlers.IntergalacticNumberDefinitionQueryHandler;
import com.machucarro.control.handlers.IntergalacticToIntConversionQueryHandler;
import com.machucarro.control.handlers.MonkeyIslandQueryHandler;
import com.machucarro.control.handlers.PriceRequestQueryHandler;
import com.machucarro.control.handlers.ProductDefinitionQueryHandler;
import com.machucarro.control.handlers.QueryHandler;
import com.machucarro.control.handlers.RouterQueryHandler;
import com.machucarro.financials.ProductPortfolio;
import com.machucarro.financials.currency.Credit;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This is the main execution monitor class for the application. It makes the interface with the user through command
 * line. The design of the application is made so this class could be easily replaced by any other kind of interface
 * such as a servlet, GUI, REST interface...
 *
 * @author MAchucarro
 */
public class ConsoleMerchantsGuideToTheGalaxy
{

    public static void main(String[] args) {
        System.out.println("Welcome to the Merchant's Guide To The Galaxy");
        System.out.println("Please, make your queries");
        Scanner scanner = new Scanner(new InputStreamReader(System.in));

        QueryHandler router = configurePipeline();

        while (scanner.hasNextLine()) {
            try {
                System.out.println(router.handle(scanner.nextLine()));
            } catch (ExecutionTerminationRequest executionTerminationRequest) {
                System.out.println(executionTerminationRequest.getMessage());
                break;
            } catch (QueryProcessingException e) {
                System.out.println(e.getMessage());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Configures the portfolio and the pipeline with the different query handlers in order to process the user queries
     * @return
     */
    private static QueryHandler configurePipeline()
    {
        ProductPortfolio portfolio = new ProductPortfolio("common metals and dirt",
                Credit.getInstance());
        List<QueryHandler> handlers = new ArrayList<QueryHandler>();
        handlers.add(new ExitQueryHandler());
        handlers.add(new IntergalacticNumberDefinitionQueryHandler());
        handlers.add(new ProductDefinitionQueryHandler(portfolio));
        handlers.add(new IntergalacticToIntConversionQueryHandler());
        handlers.add(new PriceRequestQueryHandler(portfolio));
        handlers.add(new MonkeyIslandQueryHandler());
        return new RouterQueryHandler(handlers, RouterQueryHandler.ROUTING_MASK_ALL);
    }
}
