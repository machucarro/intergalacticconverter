package com.machucarro.control.handlers;

import com.machucarro.control.exceptions.QueryProcessingException;
import com.machucarro.financials.ProductPortfolio;
import com.machucarro.financials.currency.Credit;
import com.machucarro.financials.exceptions.ProductPortfolioException;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.validators.IntergalacticNumberFormatValidator;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This {@link com.machucarro.control.handlers.QueryHandler} processes the
 * requests to calculate the price of a given amount for an already registered
 * product.
 * 
 * @author MAchucarro
 */
public class PriceRequestQueryHandler implements QueryHandler {

    private String name = PriceRequestQueryHandler.class.getSimpleName();

    /**
     * This one is the {@link com.machucarro.financials.ProductPortfolio} used
     * to retrieve the products, prices and
     * {@link com.machucarro.financials.currency.UniversalCurrency} information
     */
    private ProductPortfolio portfolio;

    /**
     * Default String literals. Some are not static because they should depend
     * on the portfolio. This way we could handle in one application more than
     * one portfolio with different products and currencies.
     */
    private static final String IS = " +is +";
    private String HOW_MANY_IS;
    private String PRICE_REQUEST;
    /**
     * Regular expressions patterns
     */
    private Pattern QUERY_MATCH_PATTERN;
    private Pattern PRODUCT_NAME_PATTERN;
    private Pattern AMOUNT_PATTERN;

    /**
     * Constructs a handler for the given product portfolio also taking in mind
     * the {@link com.machucarro.financials .currency.UniversalCurrency} defined
     * in it for processing the queries.
     * 
     * @param portfolio
     */
    public PriceRequestQueryHandler(ProductPortfolio portfolio) {
        this.portfolio = portfolio;
        HOW_MANY_IS = "how many " + portfolio.getCurrency().getPluralName() + IS;
        PRICE_REQUEST = HOW_MANY_IS + IntergalacticNumberFormatValidator.NUMBER_REGEX + " +"
                + ProductPortfolio.PRODUCT_NAME_REGEX + REQUEST_TERMINATOR;
        QUERY_MATCH_PATTERN = Pattern.compile(PRICE_REQUEST, Pattern.CASE_INSENSITIVE);

        /**
         * The following two patterns could be improved to exactly match the
         * product names and number units defined in the portfolio and the
         * intergalactic number system. Then they should be calculated in the
         * handle() method
         */
        PRODUCT_NAME_PATTERN = Pattern.compile("(" + ProductPortfolio.PRODUCT_NAME_REGEX + ")"
                + REQUEST_TERMINATOR, Pattern.CASE_INSENSITIVE);
        AMOUNT_PATTERN = Pattern.compile(HOW_MANY_IS + "("
                + IntergalacticNumberFormatValidator.NUMBER_REGEX + ") +"
                + ProductPortfolio.PRODUCT_NAME_REGEX + REQUEST_TERMINATOR,
                Pattern.CASE_INSENSITIVE);
    }

    /**
     * @param query
     *            "how many [currency plural] is [amount] [product]?" for
     *            example "how many Credits is glob prok Gold?"
     * @return "[amount] [product] is [formatted price]" for example
     *         "glob prok Gold is 57800 Credits"
     * @throws ProductPortfolioException
     * @throws QueryProcessingException
     *             if the intergalactic number cannot be extracted from the
     *             query
     */
    @Override
    public String handle(String query) throws ProductPortfolioException, QueryProcessingException {
        String product;
        Matcher productMatcher = PRODUCT_NAME_PATTERN.matcher(query);
        if (productMatcher.find()) {
            product = productMatcher.group(1);
        } else {
            throw new QueryProcessingException(
                    "An error occured when trying to process the query. Could not extract "
                            + "the product name.", this);
        }

        String amountS;
        Matcher amountMatcher = AMOUNT_PATTERN.matcher(query);
        if (amountMatcher.find()) {
            amountS = amountMatcher.group(1);
        } else {
            throw new QueryProcessingException(
                    "An error occured when trying to process the query. Could not extract "
                            + "the amount of product.", this);
        }

        IntergalacticNumber amount = new IntergalacticNumber(amountS);
        BigDecimal totalPrice = portfolio.getPriceFor(product, amount.bigDecimalValue());
        String response = amount + " " + product + " is "
                + Credit.getInstance().formatAmount(totalPrice);
        return response;
    }

    /**
     * @see QueryHandler#canHandle(String)
     */
    @Override
    public boolean canHandle(String query) {
        return QUERY_MATCH_PATTERN.matcher(query).matches();
    }

    /**
     * @see QueryHandler#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
