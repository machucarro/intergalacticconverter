package com.machucarro.control.handlers;

import com.machucarro.numerical.exceptions.IntergalacticNumberFormatException;
import com.machucarro.numerical.exceptions.RomanNumberFormatException;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.numbers.RomanNumber;
import com.machucarro.numerical.validators.IntergalacticNumberFormatValidator;
import com.machucarro.numerical.validators.RomanNumberFormatValidator;

import java.util.regex.Pattern;

/**
 * This {@link com.machucarro.control.handlers.QueryHandler} processes the
 * queries which define the equivalence of numbers between the intergalactic
 * system and the roman one.
 * 
 * @author MAchucarro
 */
public class IntergalacticNumberDefinitionQueryHandler implements QueryHandler {
    private static final String NAME = IntergalacticNumberDefinitionQueryHandler.class
            .getSimpleName();

    /**
     * String literals
     */
    private static final String IS = " +is +";
    private static final String INTERGALACTIC_NUMBER_DEFINITION = IntergalacticNumberFormatValidator.DIGIT_REGEX
            + IS + RomanNumberFormatValidator.DIGIT_REGEX;

    /**
     * Regular expression patterns
     */
    private static final Pattern QUERY_MATCH_PATTERN = Pattern.compile(
            INTERGALACTIC_NUMBER_DEFINITION, Pattern.CASE_INSENSITIVE);

    /**
     * Processes the query to extract the new intergalactic number digit and its
     * roman equivalent and registers the corresponding equivalence in the
     * intergalactic number system
     * 
     * @param query
     *            "[intergalactic digit] is [roman digit]" for example
     *            "prok is V"
     * @return an empty string
     * @throws IntergalacticNumberFormatException
     *             see
     *             {@link com.machucarro.numerical.numbers.IntergalacticNumber}
     * @throws RomanNumberFormatException
     *             see {@link com.machucarro.numerical.numbers.RomanNumber}
     */
    @Override
    public String handle(String query) throws RomanNumberFormatException,
            IntergalacticNumberFormatException {
        String intergalacticDigit = query.split(IS)[0];
        String romanDigit = query.split(IS)[1];
        IntergalacticNumber.addDigit(intergalacticDigit, new RomanNumber(romanDigit.toUpperCase()));
        return "";
    }

    /**
     * @see QueryHandler#canHandle(String)
     */
    @Override
    public boolean canHandle(String query) {
        return QUERY_MATCH_PATTERN.matcher(query).matches();
    }

    /**
     * @see QueryHandler#getName()
     */
    @Override
    public String getName() {
        return NAME;
    }

}
