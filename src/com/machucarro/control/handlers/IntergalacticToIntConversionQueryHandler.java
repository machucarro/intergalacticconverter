package com.machucarro.control.handlers;

import com.machucarro.control.exceptions.QueryProcessingException;
import com.machucarro.numerical.exceptions.IntergalacticNumberFormatException;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.validators.IntergalacticNumberFormatValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This {@link com.machucarro.control.handlers.QueryHandler} processes the
 * requests to calculate the conversion of intergalactic numbers to integer.
 * 
 * @author MAchucarro
 */
public class IntergalacticToIntConversionQueryHandler implements QueryHandler {
    private static final String NAME = IntergalacticToIntConversionQueryHandler.class
            .getSimpleName();

    /**
     * String literals
     */
    private static final String HOW_MUCH_IS = "how much is +";
    private static final String INTERGALACTIC_TO_INT_CONVERSION = HOW_MUCH_IS
            + IntergalacticNumberFormatValidator.NUMBER_REGEX + REQUEST_TERMINATOR;

    /**
     * Regular expressions patterns
     */
    private static final Pattern QUERY_MATCH_PATTERN = Pattern.compile(
            INTERGALACTIC_TO_INT_CONVERSION, Pattern.CASE_INSENSITIVE);
    private static final Pattern INTERGALACTIC_NUMBER_FIND_PATTERN = Pattern.compile(HOW_MUCH_IS
            + "(" + IntergalacticNumberFormatValidator.NUMBER_REGEX + ")" + REQUEST_TERMINATOR,
            Pattern.CASE_INSENSITIVE);

    /**
     * Processes the query to extract the new intergalactic number which wants
     * to be converted to integer and calculates it
     * 
     * @param query
     *            "how much is [intergalactic number]?" for example
     *            "how much is pish tegj glob glob ?"
     * @return "[intergalactic number] is [int equivalent]" for example
     *         "pish tegj glob glob is 42"
     * @throws QueryProcessingException
     *             if the intergalactic number cannot be extracted from the
     *             query
     * @throws IntergalacticNumberFormatException
     *             see
     *             {@link com.machucarro.numerical.numbers.IntergalacticNumber}
     */
    @Override
    public String handle(String query) throws QueryProcessingException,
            IntergalacticNumberFormatException {
        IntergalacticNumber number;
        Matcher intergalacticNumberMatcher = INTERGALACTIC_NUMBER_FIND_PATTERN.matcher(query);
        if (intergalacticNumberMatcher.find()) {
            number = new IntergalacticNumber(intergalacticNumberMatcher.group(1).trim());
            return number + " is " + number.intValue();
        } else {
            throw new QueryProcessingException(
                    "An error occured when trying to process the query.", this);
        }
    }

    /**
     * @see QueryHandler#canHandle(String)
     */
    @Override
    public boolean canHandle(String query) {
        return QUERY_MATCH_PATTERN.matcher(query).matches();
    }

    /**
     * @see QueryHandler#getName()
     */
    @Override
    public String getName() {
        return NAME;
    }
}
