package com.machucarro.control.handlers;

import com.machucarro.control.exceptions.ExecutionTerminationRequest;
import com.machucarro.control.exceptions.QueryProcessingException;

/**
 * This interface defines a basic execution element for building a query
 * processing pipeline. This allows this "pipeline framework" to be easily
 * configured by context files and annotations when working with frameworks like
 * Spring and allows multiple implementations of different execution pipes for
 * different purposes. It's inspired by the concept of other pipeline frameworks
 * like bash or Spring Integration. It provides a high flexibility, reusability
 * and configurability.
 * 
 * @author MAchucarro
 */
public interface QueryHandler {
    /**
     * String literal for identifying the final question mark of a request query
     */
    public static final String REQUEST_TERMINATOR = " *\\?";

    /**
     * Processes the given query and gives back a string. This method is where
     * the core functionality of the handlers should lay.
     * 
     * @param query
     * @return
     * @throws Exception
     *             if any exception happens during the process
     * @throws ExecutionTerminationRequest
     *             if the handler wants to notify the execution monitor class to
     *             interrupt program execution
     * @throws QueryProcessingException
     *             if any parameter cannot be correctly processed or extracted
     *             from the query
     */
    public String handle(String query) throws Exception, ExecutionTerminationRequest,
            QueryProcessingException;

    /**
     * This method checks if a handler is able to process a given query
     * 
     * @param query
     * @return true if the handler can process the query, false otherwise
     */
    public boolean canHandle(String query);

    /**
     * Some handlers may have different names and this could be useful for
     * dynamic execution pipelines or for having more than one instance of
     * certain handlers with different configurations. It?s also used for the
     * {@link QueryProcessingException} and could be used as well for the
     * {@link ExecutionTerminationRequest}
     * 
     * @return
     */
    public String getName();
}
