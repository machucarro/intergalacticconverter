package com.machucarro.control.handlers;

import com.machucarro.control.exceptions.ExecutionTerminationRequest;

/**
 * This handler is an execution flow control handler used for terminating the
 * application when a query with the command "exit" is read. It throws an
 * {@link com.machucarro.control.exceptions.ExecutionTerminationRequest} which
 * should be captured by the monitor class which handles the execution of the
 * program in order to terminate it in a controlled manner.
 * 
 * @author MAchucarro
 */
public class ExitQueryHandler implements QueryHandler {
    private static final String EXIT = "exit";
    private static final String GOODBYE_MESSAGE = "Bye bye!";
    private static final String NAME = "ExitCommandHandler";

    /**
     * Throws a notification to terminate the program execution
     * 
     * @param query
     *            is ignored
     * @return nothing
     * @throws com.machucarro.control.exceptions.ExecutionTerminationRequest
     *             with a goodbye message
     */
    @Override
    public String handle(String query) throws ExecutionTerminationRequest {
        throw new ExecutionTerminationRequest(GOODBYE_MESSAGE);
    }

    /**
     * @see QueryHandler#canHandle(String)
     */
    @Override
    public boolean canHandle(String query) {
        return query.trim().matches(EXIT);
    }

    /**
     * @see QueryHandler#getName()
     */
    @Override
    public String getName() {
        return NAME;
    }
}
