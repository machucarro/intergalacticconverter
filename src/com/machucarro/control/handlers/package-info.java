/**
 *
 * This package contains the basic elements of the execution pipeline framework. The elements in this package may be
 * interchanged and interconnected to create different execution pipelines to process String messages.
 *
 * @see com.machucarro.control
 * @author MAchucarro
 */
package com.machucarro.control.handlers;