package com.machucarro.control.handlers;

import com.machucarro.control.exceptions.ExecutionTerminationRequest;
import com.machucarro.control.exceptions.QueryProcessingException;

import java.util.ArrayList;
import java.util.List;

/**
 * This handler is used to route the query to other handlers which can process
 * it. Several RouterHandlers may be used with different query masks in order to
 * create more complex execution pipelines. The routerHandler may be easily
 * configured by context management frameworks like Spring to fit the needs of
 * any configuration without the need of changing the classes and recompile. It
 * may also change dynamically it's routing mask during runtime allowing for
 * adapting the execution pipeline to each situation that may arise.
 * 
 * @author MAchucarro
 */
public class RouterQueryHandler implements QueryHandler {

    private String name = IntergalacticToIntConversionQueryHandler.class.getSimpleName();

    // List to allow more than one handler for same query and execution in order
    // of registration
    private List<QueryHandler> queryHandlers = new ArrayList<QueryHandler>();
    private static final String CANT_ROUTE_MESSAGE = "I have no idea what you are talking about";
    public static final String ROUTING_MASK_ALL = ".*";
    private String routingMask = ROUTING_MASK_ALL;

    /**
     * Creates a new RouterHandler which filters the queries according to the
     * given routingMask and asks all the handlers "connected" to it if they can
     * handle the received query.
     * 
     * @param queryHandlers
     *            List of handlers to "connect" or register to this router
     * @param routingMask
     *            Regular Expression that will be used to determine if this
     *            router should route the query
     */
    public RouterQueryHandler(List<QueryHandler> queryHandlers, String routingMask) {
        this.queryHandlers = queryHandlers;
        this.routingMask = routingMask;
    }

    /**
     * Goes through the registered handlers list and forwards the query to the
     * first one it finds that can handle it
     * 
     * @param query
     *            the query to route
     * @return the return value of the first handler which can handle the query
     * @throws Exception
     *             if any exception is thrown by the handler which processes the
     *             query
     * @throws ExecutionTerminationRequest
     *             if the handler which processes the query launches a
     *             termination request
     */
    @Override
    public String handle(String query) throws Exception, ExecutionTerminationRequest,
            QueryProcessingException {
        if (query == null || query.isEmpty()) {
            return "";
        }
        query = query.trim();
        for (QueryHandler queryHandler : queryHandlers) {
            if (queryHandler.canHandle(query)) {
                return queryHandler.handle(query);
            }
        }
        return CANT_ROUTE_MESSAGE;
    }

    /**
     * @see QueryHandler#canHandle(String)
     */
    @Override
    public boolean canHandle(String query) {
        return query.matches(routingMask);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoutingMask(String queryMaskRegex) {
        this.routingMask = queryMaskRegex;
    }

    public void setQueryHandlers(List<QueryHandler> queryHandlers) {
        this.queryHandlers = queryHandlers;
    }
}
