package com.machucarro.control.handlers;

import java.util.regex.Pattern;

/**
 * This {@link QueryHandler} is used to trigger the nostalgy and frikism for the
 * cool old graphic adventure classic "Monkey Island 2: LeChuck's Revenge". It
 * allows you to recall the conversation of Guybrush Threepwood with the
 * woodchuck about the woodckuck riddle :)
 * 
 * @author MAchucarro
 */
public class MonkeyIslandQueryHandler implements QueryHandler {
    private static final String NAME = MonkeyIslandQueryHandler.class.getSimpleName();

    /**
     * String literals
     */
    private static final String MONKEY_ISLAND_GUYBRUSH_QUESTION_1 = "how much wood could a woodchuck chuck if a woodchuck could chuck wood"
            + REQUEST_TERMINATOR;
    private static final String MONKEY_ISLAND_WOODCHUCK_ANSWER_1 = "Woodchuck: A woodchuck could chuck no amount of wood since a woodchuck can't chuck wood.";
    private static final String MONKEY_ISLAND_GUYBRUSH_QUESTION_2 = "But if a woodchuck could chuck and would chuck some amount of wood, Show much wood would a woodchuck "
            + "chuck" + REQUEST_TERMINATOR;
    private static final String MONKEY_ISLAND_WOODCHUCK_ANSWER_2 = "Woodchuck: Even if a woodchuck could chuck wood, and even if a woodchuck would chuck wood, "
            + "should a woodchuck chuck wood?";
    private static final String MONKEY_ISLAND_GUYBRUSH_QUESTION_3 = "A woodchuck should chuck if a woodchuck could chuck wood, as long as a woodchuck would chuck wood";
    private static final String MONKEY_ISLAND_WOODCHUCK_ANSWER_3 = "Woodchuck: Oh. Shut up.";

    private static final String MONKEY_ISLAND_WOODCHUCK_ANSWER_NOT_FOUND = "I am rubber, you are glue...";

    /**
     * Regex patterns
     */
    private static final Pattern REGEX_GUYBRUSH_QUESTION_1 = Pattern.compile(
            MONKEY_ISLAND_GUYBRUSH_QUESTION_1, Pattern.CASE_INSENSITIVE);
    private static final Pattern REGEX_GUYBRUSH_QUESTION_2 = Pattern.compile(
            MONKEY_ISLAND_GUYBRUSH_QUESTION_2, Pattern.CASE_INSENSITIVE);
    private static final Pattern REGEX_GUYBRUSH_QUESTION_3 = Pattern.compile(
            MONKEY_ISLAND_GUYBRUSH_QUESTION_3, Pattern.CASE_INSENSITIVE);
    private static final Pattern QUERY_MATCH_PATTERN = Pattern.compile(
            MONKEY_ISLAND_GUYBRUSH_QUESTION_1 + "|" + MONKEY_ISLAND_GUYBRUSH_QUESTION_2 + "|"
                    + MONKEY_ISLAND_GUYBRUSH_QUESTION_3, Pattern.CASE_INSENSITIVE);

    /**
     * It analyses the query in order to identify to which of the questions that
     * Guybrush states in the conversation corresponds and gives the proper
     * answer from the Woodchuck.
     * 
     * @param query
     *            one of guybrush questions. You don't know them? Well, then you
     *            should play more classics!... or just ask google ;D
     * @return
     */
    @Override
    public String handle(String query) {
        if (REGEX_GUYBRUSH_QUESTION_1.matcher(query).matches()) {
            return MONKEY_ISLAND_WOODCHUCK_ANSWER_1;
        } else if (REGEX_GUYBRUSH_QUESTION_2.matcher(query).matches()) {
            return MONKEY_ISLAND_WOODCHUCK_ANSWER_2;
        } else if (REGEX_GUYBRUSH_QUESTION_3.matcher(query).matches()) {
            return MONKEY_ISLAND_WOODCHUCK_ANSWER_3;
        } else {
            return MONKEY_ISLAND_WOODCHUCK_ANSWER_NOT_FOUND;
        }
    }

    /**
     * @see QueryHandler#canHandle(String)
     */
    @Override
    public boolean canHandle(String query) {
        return QUERY_MATCH_PATTERN.matcher(query).matches();
    }

    /**
     * @see QueryHandler#getName()
     */
    @Override
    public String getName() {
        return NAME;
    }
}
