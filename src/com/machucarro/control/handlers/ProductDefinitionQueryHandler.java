package com.machucarro.control.handlers;

import com.machucarro.control.exceptions.QueryProcessingException;
import com.machucarro.financials.ProductPortfolio;
import com.machucarro.financials.exceptions.ProductPortfolioException;
import com.machucarro.numerical.exceptions.IntergalacticNumberFormatException;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.validators.IntergalacticNumberFormatValidator;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This {@link com.machucarro.control.handlers.QueryHandler} processes the
 * queries which insert new entries of products and prices to the product
 * portfolio or updates the existing ones.
 * 
 * @author MAchucarro
 */
public class ProductDefinitionQueryHandler implements QueryHandler {
    private String name = ProductDefinitionQueryHandler.class.getSimpleName();

    /**
     * String literals
     */
    private static final String IS = " +is +";
    private String PRODUCT_DEFINITION;
    private ProductPortfolio portfolio;

    /**
     * Regular expression patterns
     */
    private Pattern QUERY_MATCH_PATTERN;
    private Pattern PRODUCT_NAME_PATTERN;
    private Pattern AMOUNT_PATTERN;
    private Pattern PRICE_PATTERN;

    /**
     * Constructs a handler for the given product portfolio also taking in mind
     * the {@link com.machucarro.financials .currency.UniversalCurrency} defined
     * in it for processing the queries.
     * 
     * @param portfolio
     */
    public ProductDefinitionQueryHandler(ProductPortfolio portfolio) {
        this.portfolio = portfolio;
        PRODUCT_DEFINITION = IntergalacticNumberFormatValidator.NUMBER_REGEX + " "
                + ProductPortfolio.PRODUCT_NAME_REGEX + IS + portfolio.getCurrency().getRegex();
        QUERY_MATCH_PATTERN = Pattern.compile(PRODUCT_DEFINITION, Pattern.CASE_INSENSITIVE);
        PRODUCT_NAME_PATTERN = Pattern.compile(
                "(" + ProductPortfolio.PRODUCT_NAME_REGEX + ")" + IS, Pattern.CASE_INSENSITIVE);
        AMOUNT_PATTERN = Pattern.compile("(" + IntergalacticNumberFormatValidator.NUMBER_REGEX
                + ") +" + ProductPortfolio.PRODUCT_NAME_REGEX + IS, Pattern.CASE_INSENSITIVE);
        PRICE_PATTERN = Pattern.compile(IS + "(" + portfolio.getCurrency().getRegex() + ") *$",
                Pattern.CASE_INSENSITIVE);
    }

    /**
     * Processes the query, extract the amount of product, the name and the
     * price, claculates a unitary price and inserts a new entry or updates an
     * existing one for the defined product in the product portfolio.
     * 
     * @param query
     *            "[amount] [product] is [formatted price]" for example
     *            "glob prok Gold is 57800 Credits"
     * @return
     * @throws Exception
     * @throws QueryProcessingException
     */
    @Override
    public String handle(String query) throws ParseException, IntergalacticNumberFormatException,
            QueryProcessingException, ProductPortfolioException {
        String amountS;
        Matcher amountMatcher = AMOUNT_PATTERN.matcher(query);
        if (amountMatcher.find()) {
            amountS = amountMatcher.group(1);
        } else {
            throw new QueryProcessingException(
                    "An error occured when trying to process the query. Could not extract "
                            + "the amount.", this);
        }

        String product;
        Matcher productMatcher = PRODUCT_NAME_PATTERN.matcher(query);
        if (productMatcher.find()) {
            product = productMatcher.group(1);
        } else {
            throw new QueryProcessingException(
                    "An error occured when trying to process the query. Could not extract "
                            + "the product name.", this);
        }

        String priceS;
        Matcher priceMatcher = PRICE_PATTERN.matcher(query);
        if (priceMatcher.find()) {
            priceS = priceMatcher.group(1);
        } else {
            throw new QueryProcessingException(
                    "An error occured when trying to process the query. Could not extract "
                            + "the price.", this);
        }
        BigDecimal price = portfolio.getCurrency().parseAmount(priceS);
        BigDecimal productAmount = (new IntergalacticNumber(amountS)).bigDecimalValue();
        portfolio.setPriceFor(product, price.divide(productAmount));
        return "";
    }

    /**
     * @see QueryHandler#canHandle(String)
     */
    @Override
    public boolean canHandle(String query) {
        return QUERY_MATCH_PATTERN.matcher(query).matches();
    }

    /**
     * @see QueryHandler#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
