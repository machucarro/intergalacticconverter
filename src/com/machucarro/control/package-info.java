/**
 *
 * This package contains the execution pipeline framework for processing string messages. It provides a flexible and
 * easy to extend object structure for creating customized String message processing pipelines. In this application
 * we use it for processing the queries of the user.
 *
 * @author MAchucarro
 */
package com.machucarro.control;