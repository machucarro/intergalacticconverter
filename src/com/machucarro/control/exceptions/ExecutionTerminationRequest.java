package com.machucarro.control.exceptions;

/**
 * This throwable is used to propagate the request from an processing unit (
 * {@link com.machucarro.control.handlers .QueryHandler}) for terminate the
 * execution of the program up in the pipeline hierarchy until the execution
 * monitor or any other agent which should handle that kind of request.
 * 
 * @author MAchucarro
 */
public class ExecutionTerminationRequest extends Throwable {
    public ExecutionTerminationRequest(String message) {
        super(message);
    }
}
