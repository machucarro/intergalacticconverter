/**
 *
 * This package contains the Exceptions and Throwables related to the execution control package
 *
 * @see com.machucarro.control
 * @author MAchucarro
 */
package com.machucarro.control.exceptions;