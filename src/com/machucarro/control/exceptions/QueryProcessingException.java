package com.machucarro.control.exceptions;

import com.machucarro.control.handlers.QueryHandler;

/**
 * This exception happens when any of the parameters necessary to process a
 * query could not be extracted or when the processor capable to handle the
 * query could not do it for any reason
 * 
 * @author MAchucarro
 */
public class QueryProcessingException extends Throwable {
    public QueryProcessingException(String message, QueryHandler handler) {
        super(handler.getName() + ": " + message);
    }
}
