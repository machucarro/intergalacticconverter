package com.machucarro.financials.exceptions;

/**
 * @author MAchucarro
 */
public class ProductPortfolioException extends Exception {
    public ProductPortfolioException(String message) {
        super(message);
    }
}
