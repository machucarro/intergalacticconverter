/**
 * this package contains the financial objects and tools necessary to manage a product portfolio
 *
 * @author MAchucarro
 */
package com.machucarro.financials;