package com.machucarro.financials.currency;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * It's a singleton that contains the information of the Credit currency.
 * 
 * @author MAchucarro
 */
public class Credit implements UniversalCurrency {
    private static Credit creditInstance = new Credit();
    private final String singleName = "Credit";
    private final String pluralName = "Credits";
    private final String description = "The money of the universe!";
    private final String currencyCode = "UCR";
    private final String symbol = "Cr";
    private final int defaultFractionDigits = 2;
    private final NumberFormat numberFormat = new DecimalFormat("#.#");
    private final String regex = "(\\d+\\.{0,1}\\d* (" + singleName + "|" + pluralName + "))";

    /**
     * Gives the singleton instance of the Credit currency. It would be probably
     * better to get the instance of this class through a factory method of it's
     * super class {@link com.machucarro.financials.currency .UniversalCurrency}
     * but was not necessary for this application.
     * 
     * @return the singleton instance of Credit UniversalCurrency
     */
    public static Credit getInstance() {
        return creditInstance;
    }

    /**
     * Cannot be instantiated (singleton)
     */
    private Credit() {
    }

    /**
     * @see UniversalCurrency#getSingleName()
     * @return
     */
    @Override
    public String getSingleName() {
        return singleName;
    }

    /**
     * @see UniversalCurrency#getPluralName()
     * @return
     */
    @Override
    public String getPluralName() {
        return pluralName;
    }

    /**
     * @see UniversalCurrency#getDescription()
     * @return
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * @see UniversalCurrency#getCurrencyCode()
     * @return
     */
    @Override
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * @see UniversalCurrency#getSymbol()
     * @return
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * @see UniversalCurrency#getDefaultFractionDigits()
     * @return
     */
    @Override
    public int getDefaultFractionDigits() {
        return defaultFractionDigits;
    }

    /**
     * @see UniversalCurrency#getRegex()
     * @return
     */
    @Override
    public String getRegex() {
        return regex;
    }

    /**
     * Takes a BigDecimal or any other number extending BigDecimal and composes
     * the formatted currency string
     * 
     * @param amount
     *            BigDecimal amount
     * @param <N>
     *            Class extending {@link BigDecimal}
     * @return The corresponding formatted amount, with the currency name in
     *         plural or singular depending on the amount being bigger than
     *         {@link java.math.BigDecimal#ONE} or not.
     */
    @Override
    public <N extends BigDecimal> String formatAmount(N amount) {
        BigDecimal rounded = amount.setScale(this.defaultFractionDigits, BigDecimal.ROUND_CEILING);
        String formattedAmount = numberFormat.format(rounded);
        if (rounded.compareTo(BigDecimal.ONE) > 0) {
            formattedAmount += " " + pluralName;
        } else {
            formattedAmount += " " + singleName;
        }
        return formattedAmount;
    }

    /**
     * Extracts from a formatted currency amount the BigDecimal or any other
     * number extending BigDecimal
     * 
     * @param amount
     *            formatted currency amount, for example "1500 Credits"
     * @return The instance of N containing the BigDecimal value
     * @throws ParseException
     *             if the ammount could not be parsed to a BigDecimal
     */
    @Override
    public BigDecimal parseAmount(String amount) throws ParseException {
        String number = amount.replaceAll("(" + singleName + "|" + pluralName + ")", "").trim();
        number = number.replaceAll("\\,", ".");
        return new BigDecimal(numberFormat.parse(number).doubleValue());
    }
}
