package com.machucarro.financials.currency;

import java.math.BigDecimal;
import java.text.ParseException;

/**
 * Custom Currency interface designed for customizing currency types because
 * Java Currency does not handle locals from the whole Universe!
 * 
 * @author MAchucarro
 */
public interface UniversalCurrency {
    /**
     * Short description of the currency
     * 
     * @return
     */
    public String getDescription();

    /**
     * Number of decimal digits used in its String representation
     * 
     * @return
     */
    public int getDefaultFractionDigits();

    /**
     * Identification code (for similarity with java's Currency
     * 
     * @return
     */
    public String getCurrencyCode();

    /**
     * Short symbol
     * 
     * @return
     */
    public String getSymbol();

    /**
     * Name used for quantities whose absolute value is less or equal to 1
     * 
     * @return
     */
    public String getSingleName();

    /**
     * Name used for quantities whose absolute value is bigger than 1
     * 
     * @return
     */
    public String getPluralName();

    /**
     * Returns a properly formatted String for the given amount
     * 
     * @param amount
     * @param <N>
     * @return
     */
    public <N extends BigDecimal> String formatAmount(N amount);

    /**
     * Extracts the amount from a properly formatted string
     * 
     * @param amount
     * @return
     * @throws ParseException
     */
    public BigDecimal parseAmount(String amount) throws ParseException;

    /**
     * Gives the regex used to parse amounts expressed in this currency
     * 
     * @return
     */
    public String getRegex();
}
