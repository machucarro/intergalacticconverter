/**
 * This package contains the classes that deal with the currency issues of the financial package
 *
 * @see com.machucarro.financials
 * @author MAchucarro
 */
package com.machucarro.financials.currency;