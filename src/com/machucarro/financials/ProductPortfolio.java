package com.machucarro.financials;

import com.machucarro.financials.currency.UniversalCurrency;
import com.machucarro.financials.exceptions.ProductPortfolioException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represents a portfolio of products. It contains a list with all
 * the product names and their unitary prices. It also contains the information
 * of the {@link com.machucarro.financials.currency.UniversalCurrency} in which
 * the prices are specified. This last point was not necessary for this
 * application but it gives lots of flexibility and reusability to all the other
 * structures and classes which work with this protfolios.
 * 
 * @author MAchucarro
 */
public class ProductPortfolio {

    private final String name;
    private final UniversalCurrency currency;
    private Map<String, BigDecimal> products = new HashMap<String, BigDecimal>();
    public static final String PRODUCT_NAME_REGEX = "[A-Za-z]+";

    /**
     * Creates an empty product portfolio with the name and currency information
     * provided
     * 
     * @param name
     * @param currency
     */
    public ProductPortfolio(String name, UniversalCurrency currency) {
        super();
        this.name = name;
        this.currency = currency;
    }

    /**
     * Is the method used for querying the portfolio
     * 
     * @param productName
     *            Name of the product whose price we want to know
     * @param quantity
     *            Amount of product for which we want to calculate the price
     * @return total price for the given amount of product measured in the
     *         protfolio's currency units
     * @throws ProductPortfolioException
     *             if the given product is not defined in the portfolio
     */
    public BigDecimal getPriceFor(String productName, BigDecimal quantity)
            throws ProductPortfolioException {
        BigDecimal price = products.get(productName);
        if (price == null) {
            throw new ProductPortfolioException("The product " + productName
                    + " was not found in the product " + "portfolio. ");
        }
        return quantity.multiply(price);
    }

    /**
     * This is the method used for registering a new product in the portfolio or
     * updating the price of an existing one
     * 
     * @param productName
     *            name of the product to update or insert
     * @param price
     *            unitary price for this product
     * @return the same price object if the insertion was successful
     * @throws ProductPortfolioException
     *             if the name of the product doesn't meet the product name
     *             format rules
     */
    public BigDecimal setPriceFor(String productName, BigDecimal price)
            throws ProductPortfolioException {
        if (!productName.matches(PRODUCT_NAME_REGEX)) {
            throw new ProductPortfolioException(productName + " is not a valid product name");
        }
        return products.put(productName, price);
    }

    /**
     * Method used for removing entries from the portfolio
     * 
     * @param productName
     * @return the removed price in case of successful deletion, null if the
     *         produc was not found
     */
    public BigDecimal removePriceFor(String productName) {
        return products.remove(productName);
    }

    /**
     * retrieves the full map of products and prices
     * 
     * @return
     */
    public Map<String, BigDecimal> getProducts() {
        return products;
    }

    /**
     * sets and overwrites the full map of products and prices
     * 
     * @param products
     */
    public void setProducts(Map<String, BigDecimal> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        String portfolio = name + ":\n";
        for (Map.Entry<String, BigDecimal> productEntry : products.entrySet()) {
            portfolio += productEntry.getKey() + " => "
                    + currency.formatAmount(productEntry.getValue()) + "\n";
        }
        return portfolio;
    }

    public String getName() {
        return name;
    }

    public UniversalCurrency getCurrency() {
        return currency;
    }
}
