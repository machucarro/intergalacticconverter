/**
 * This package contains the classes and tools necessary to deal with custom number-related issues
 *
 * @author MAchucarro
 */
package com.machucarro.numerical;