package com.machucarro.numerical.numbers;

import java.math.BigDecimal;

/**
 * 
 * Interface subclass of Number which pretends to force the usage of BigDecimal
 * as financial operations format as it's round safe for this purposes.
 * 
 * @author MAchucarro
 */
public abstract class UniversalFinanceNumber extends Number {
    /**
     * Gives the BigDecimal representation of the UniversalFinanceNumber
     * 
     * @return
     */
    public abstract BigDecimal bigDecimalValue();

    /**
     * @see Object#toString()
     * @return
     */
    public abstract String toString();
}
