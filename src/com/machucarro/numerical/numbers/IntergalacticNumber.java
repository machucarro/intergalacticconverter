package com.machucarro.numerical.numbers;

import com.machucarro.numerical.converters.IntergalacticToRomanConverter;
import com.machucarro.numerical.exceptions.IntergalacticNumberFormatException;
import com.machucarro.numerical.validators.IntergalacticNumberFormatValidator;
import com.machucarro.numerical.validators.RomanNumberFormatValidator;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * This class represents an intergalactic number suitable to be used for
 * financial purpopses as it also provides its BigDecimal format. This class
 * constructs its set of basic digits dynamically as it should be set by the
 * user at runtime
 * 
 * @author MAchucarro
 */
public class IntergalacticNumber extends UniversalFinanceNumber {
    private String stringValue;
    private RomanNumber romanValue;

    /**
     * Validator used for checking the format of the new IntergalacticNumber
     */
    private IntergalacticNumberFormatValidator validator = new IntergalacticNumberFormatValidator();

    /**
     * Set of the single-digit IntergalacticNumber that may be used to form
     * other numbers
     */
    private static Set<IntergalacticNumber> DIGITS = new HashSet<IntergalacticNumber>();

    /**
     * Creates a new IntergalacticNumber from the given String
     * 
     * @param stringValue
     * @throws IntergalacticNumberFormatException
     *             if the given String is not a valid intergalacticNumber with
     *             the current set of
     *             {@link com.machucarro.numerical.numbers.IntergalacticNumber#DIGITS}
     */
    public IntergalacticNumber(String stringValue) throws IntergalacticNumberFormatException {
        this.stringValue = stringValue;
        validator.validate(this);
    }

    /**
     * Private unchecked constructor used for adding digits to the digit set
     * 
     * @param stringValue
     * @param romanValue
     */
    private IntergalacticNumber(String stringValue, RomanNumber romanValue) {
        this.stringValue = stringValue;
        this.romanValue = romanValue;
    }

    /**
     * @see UniversalFinanceNumber#bigDecimalValue()
     * @return
     */
    @Override
    public BigDecimal bigDecimalValue() {
        return BigDecimal.valueOf(this.intValue());
    }

    /**
     * @see UniversalFinanceNumber#toString()
     * @return
     */
    @Override
    public String toString() {
        return stringValue;
    }

    /**
     * @see UniversalFinanceNumber#intValue()
     * @return
     */
    @Override
    public int intValue() {
        return romanValue().intValue();
    }

    /**
     * Gives the {@link com.machucarro.numerical.numbers.RomanNumber}
     * representation of the IntergalacticNumber
     * 
     * @return
     */
    public RomanNumber romanValue() {
        if (this.romanValue == null) {
            this.romanValue = IntergalacticToRomanConverter.convert(this);
        }
        return romanValue;
    }

    /**
     * @see UniversalFinanceNumber#longValue()
     * @return
     */
    @Override
    public long longValue() {
        return Long.valueOf(this.intValue());
    }

    /**
     * @see UniversalFinanceNumber#floatValue()
     * @return
     */
    @Override
    public float floatValue() {
        return Float.valueOf(this.intValue());
    }

    /**
     * @see UniversalFinanceNumber#doubleValue()
     * @return
     */
    @Override
    public double doubleValue() {
        return Double.valueOf(this.intValue());
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @param other
     * @return
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        IntergalacticNumber otherIntergalactic = other instanceof IntergalacticNumber ? ((IntergalacticNumber)other)
                : null;
        if (otherIntergalactic == null) {
            return false;
        }
        return otherIntergalactic.intValue() == this.intValue()
                && otherIntergalactic.toString().equals(this.toString());
    }

    /**
     * Gives an immutable copy of the current valid
     * {@link com.machucarro.numerical.numbers.IntergalacticNumber#DIGITS}
     * 
     * @return
     */
    public static final Set<IntergalacticNumber> getDIGITS() {
        /**
         * This method returns a new immutable instance to avoid the digits list
         * to be manipulated outside this class
         */

        final Set<IntergalacticNumber> copy = new HashSet<IntergalacticNumber>(DIGITS);
        return copy;
    }

    /**
     * Upserts a digit with its equivalent RomanNumber to the
     * {@link com.machucarro.numerical.numbers .IntergalacticNumber#DIGITS}
     * 
     * @param symbol
     *            String representation of the new digit
     * @param value
     *            RomanNumber digit equivalent
     * @return true if the addition was successful
     * @throws IntergalacticNumberFormatException
     *             if the given digit doesn't meet the format rules, contains a
     *             digit separator or the roman equivalent is not a roman digit
     */
    public final static boolean addDigit(String symbol, RomanNumber value)
            throws IntergalacticNumberFormatException {
        boolean result = false;
        if (symbol == null || symbol.isEmpty()) {
            throw new IntergalacticNumberFormatException(
                    "The intergalactic symbol for the digit equivalence must not be null or empty");
        } else if (value == null) {
            throw new IntergalacticNumberFormatException(
                    "The equivalent roman number must not be null");
        } else if (!symbol.matches(IntergalacticNumberFormatValidator.DIGIT_REGEX)) {
            throw new IntergalacticNumberFormatException(
                    "An equivalence must contain just one symbol of " + "alphanumerical chars");
        }
        if (!value.toString().matches(RomanNumberFormatValidator.DIGIT_REGEX)) {
            throw new IntergalacticNumberFormatException(
                    "The roman number value for the equivalence must be a digit");
        }
        synchronized (DIGITS) {
            result = DIGITS.add(new IntergalacticNumber(symbol, value));
        }
        return result;
    }

    /**
     * Removes a given digit from
     * {@link com.machucarro.numerical.numbers .IntergalacticNumber#DIGITS}
     * 
     * @param digit
     * @return true if the removal was successful
     */
    public static boolean removeDigit(IntergalacticNumber digit) {
        boolean result = false;
        synchronized (DIGITS) {
            result = DIGITS.remove(digit);
        }
        return result;

    }

    /**
     * Clears the list of
     * {@link com.machucarro.numerical.numbers .IntergalacticNumber#DIGITS}
     */
    public static void clearDigits() {
        DIGITS.clear();
    }
}
