package com.machucarro.numerical.numbers;

import com.machucarro.numerical.converters.RomanToIntegerConverter;
import com.machucarro.numerical.exceptions.RomanNumberFormatException;
import com.machucarro.numerical.validators.RomanNumberFormatValidator;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * This class represents a roman number suitable to be used for financial purpopses as it also provides its
 * BigDecimal format
 *
 * @author MAchucarro
 */
public class RomanNumber extends UniversalFinanceNumber {


    public static final RomanNumber I = new RomanNumber("I", 1);
    public static final RomanNumber V = new RomanNumber("V", 5);
    public static final RomanNumber X = new RomanNumber("X", 10);
    public static final RomanNumber L = new RomanNumber("L", 50);
    public static final RomanNumber C = new RomanNumber("C", 100);
    public static final RomanNumber D = new RomanNumber("D", 500);
    public static final RomanNumber M = new RomanNumber("M", 1000);

    /**
     * Set containing all the valid roman number digits
     */
    public static final Set<RomanNumber> DIGITS = new HashSet<RomanNumber>();
    static {
        DIGITS.add(I);
        DIGITS.add(V);
        DIGITS.add(X);
        DIGITS.add(L);
        DIGITS.add(C);
        DIGITS.add(D);
        DIGITS.add(M);
    }

    private String stringValue;
    private Integer intValue;

    /**
     * Validator used for checking the format of the new RomanNumber
     */
    private RomanNumberFormatValidator validator = new RomanNumberFormatValidator();

    /**
     * Creates a new RomanNumber from the given String
     * @param stringValue
     * @throws RomanNumberFormatException if the given String is not a valid roman number
     */
    public RomanNumber(String stringValue) throws RomanNumberFormatException {
        this.stringValue = stringValue;
        validator.validate(this);
    }

    /**
     * Unchecked constructor used to assign the values of the initial digits
     * @param stringValue
     * @param intValue
     */
    private RomanNumber(String stringValue, int intValue) {
        this.stringValue = stringValue;
        this.intValue = intValue;
    }

    /**
     * @see Number#intValue()
     * @return
     */
    @Override
    public int intValue() {
        if (this.intValue == null) {
            this.intValue = RomanToIntegerConverter.convert(this);
        }
        return intValue.intValue();
    }

    /**
     * @see Number#longValue()
     * @return
     */
    @Override
    public long longValue() {
        return intValue.longValue();
    }

    /**
     * @see Number#floatValue()
     * @return
     */
    @Override
    public float floatValue() {
        return intValue.floatValue();
    }

    /**
     * @see Number#doubleValue()
     * @return
     */
    @Override
    public double doubleValue() {
        return intValue.doubleValue();
    }

    /**
     * @see com.machucarro.numerical.numbers.UniversalFinanceNumber#bigDecimalValue()
     * @return
     */
    @Override
    public BigDecimal bigDecimalValue() {
        return new BigDecimal(intValue);
    }

    /**
     * @see UniversalFinanceNumber#toString()
     * @return
     */
    @Override
    public String toString() {
        return stringValue;
    }

    /**
     * @see java.lang.Object#equals(Object)
     * @param other
     * @return
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        RomanNumber otherRoman = other instanceof RomanNumber ? ((RomanNumber)other) : null;
        if (otherRoman == null) {
            return false;
        }
        return otherRoman.intValue() == this.intValue()
                && otherRoman.toString().equals(this.toString());
    }
}
