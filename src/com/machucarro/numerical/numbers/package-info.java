/**
 * This package contains the custom Number classes created for dealing with special numbers in financial purposes
 *
 * @see com.machucarro.numerical
 * @author MAchucarro
 */
package com.machucarro.numerical.numbers;