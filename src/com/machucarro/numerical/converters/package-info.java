/**
 * This package contains the classes which convert the Number objects from the package {@link com.machucarro.numerical
 * .numbers} from one type to another
 *
 * @see com.machucarro.numerical.numbers
 * @see com.machucarro.numerical
 * @author MAchucarro
 */
package com.machucarro.numerical.converters;