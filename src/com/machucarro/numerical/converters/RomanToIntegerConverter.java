package com.machucarro.numerical.converters;

import com.machucarro.numerical.exceptions.RomanNumberFormatException;
import com.machucarro.numerical.numbers.RomanNumber;
import com.machucarro.numerical.validators.RomanNumberFormatValidator;

/**
 * This class makes conversions between roman numbers and int numbers
 * 
 * @author MAchucarro
 */
public class RomanToIntegerConverter {
    /**
     * Format validator to ensure the given roman number meets the established
     * format rules
     */
    private static RomanNumberFormatValidator validator = new RomanNumberFormatValidator();

    /**
     * Converts a roman number into an arabic number (integer)
     * 
     * @param romanNumber
     * @return the Integer fot the corresponding arabic number equivalent to the
     *         roman given input
     * @throws com.machucarro.numerical.exceptions.RomanNumberFormatException
     *             if the given roman number doesn't compile with the rules
     *             stated in
     *             {@link com.machucarro.numerical.validators.RomanNumberFormatValidator#validate(String)}
     */
    public static Integer convert(String romanNumber) throws RomanNumberFormatException {
        validator.validate(romanNumber);
        Integer result = 0;

        for (int i = 0; i < romanNumber.length() - 1; i++) { // this loop will
                                                             // add each Roman
                                                             // character value
            int currentValue = convertRomanDigit(romanNumber.charAt(i));
            int nextValue = convertRomanDigit(romanNumber.charAt(i + 1));
            if (currentValue < nextValue) {
                /**
                 * As stated in the Roman numbers definition, if a value is
                 * followed by a bigger one, then this value should be
                 * substracted from the next one. as all are sums, it may be
                 * substracted from the result. For this to work properly we
                 * asume the number was previously validated to be a correctly
                 * specified Roman number
                 */
                result -= currentValue;
            } else {
                /**
                 * Otherwise it's just added to the result
                 */
                result += currentValue;
            }

        }

        result += convertRomanDigit(romanNumber.charAt(romanNumber.length() - 1));
        return result;
    }

    /**
     * Converts a roman number in String form into an arabic number (integer)
     * 
     * @param romanNumber
     * @return
     * @throws RomanNumberFormatException
     */
    public static Integer convert(RomanNumber romanNumber) throws RomanNumberFormatException {
        return convert(romanNumber.toString());
    }

    /**
     * Gives integer value for the corresponding Roman char digit
     * 
     * @param romanDigit
     * @return the integer value for the corresponding Roman String digit
     */
    public static int convertRomanDigit(char romanDigit) throws RomanNumberFormatException {
        String stringDigit = String.valueOf(romanDigit);
        for (RomanNumber r : RomanNumber.DIGITS) {
            if (stringDigit.equalsIgnoreCase(r.toString())) {
                return r.intValue();
            }
        }
        throw new RomanNumberFormatException("Unknown roman digit \"" + stringDigit + "\"");
    }
}
