package com.machucarro.numerical.converters;

import com.machucarro.numerical.exceptions.IntergalacticNumberFormatException;
import com.machucarro.numerical.numbers.IntergalacticNumber;
import com.machucarro.numerical.numbers.RomanNumber;
import com.machucarro.numerical.validators.IntergalacticNumberFormatValidator;

/**
 * This class converts intergalactic numbers to roman numbers
 * 
 * @author MAchucarro
 */
public class IntergalacticToRomanConverter {
    /**
     * Format validator to ensure the given intergalactic number meets the
     * established format rules
     */
    private static IntergalacticNumberFormatValidator validator = new IntergalacticNumberFormatValidator();

    /**
     * Calculates the equivalent RomanNumber for a given IntergalacticNumber
     * 
     * @param number
     * @return
     * @throws IntergalacticNumberFormatException
     */
    public static RomanNumber convert(IntergalacticNumber number)
            throws IntergalacticNumberFormatException {
        return convert(number.toString());
    }

    /**
     * Calculates the equivalent RomanNumber for a given IntergalacticNumber
     * expressed as String
     * 
     * @param intergalacticValue
     * @return
     * @throws IntergalacticNumberFormatException
     */
    public static RomanNumber convert(String intergalacticValue)
            throws IntergalacticNumberFormatException {
        validator.validate(intergalacticValue);
        return new RomanNumber(transliterateToRoman(intergalacticValue));
    }

    /**
     * Transliterates a given IntergalacticNumber expressed as String into roman
     * digits
     * 
     * @param intergalacticValue
     * @return
     * @throws IntergalacticNumberFormatException
     */
    public static String transliterateToRoman(String intergalacticValue)
            throws IntergalacticNumberFormatException {
        String[] interGalacticDigits = intergalacticValue
                .split(IntergalacticNumberFormatValidator.DIGIT_SEPARATOR);
        String romanTransliteration = new String(intergalacticValue);
        for (IntergalacticNumber interGalacticDigit : IntergalacticNumber.getDIGITS()) {
            romanTransliteration = romanTransliteration.replaceAll(
                    "\\b" + interGalacticDigit.toString() + "\\b", interGalacticDigit.romanValue()
                            .toString());
        }
        romanTransliteration = romanTransliteration.replaceAll(
                IntergalacticNumberFormatValidator.DIGIT_SEPARATOR, "");
        return romanTransliteration;
    }
}
