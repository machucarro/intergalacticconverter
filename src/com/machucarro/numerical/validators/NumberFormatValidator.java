package com.machucarro.numerical.validators;

/**
 * 
 * This interface represents the validators used to test and contain the
 * formatting rules established for the different number systems and
 * conversions. It's purpose is more for supporting coherence implementation of
 * the validators than for using them as parametrized objects as the
 * implementations I did are not generic typed.
 * 
 * @author MAchucarro
 */
public interface NumberFormatValidator {
    /**
     * Generics implementation of the validation method. It's used for coherence
     * purposes between the validators
     * 
     * @param value
     * @param <T>
     * @return
     */
    public <T extends Number> boolean validate(T value);
}
