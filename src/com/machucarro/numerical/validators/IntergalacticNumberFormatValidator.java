package com.machucarro.numerical.validators;

import com.machucarro.numerical.converters.IntergalacticToRomanConverter;
import com.machucarro.numerical.exceptions.IntergalacticNumberFormatException;
import com.machucarro.numerical.exceptions.RomanNumberFormatException;
import com.machucarro.numerical.numbers.IntergalacticNumber;

/**
 * 
 * This class represents the formatting rules established for the intergalactic
 * numbers and contains the regular expressions used to parse them.
 * 
 * @author MAchucarro
 */
public class IntergalacticNumberFormatValidator implements NumberFormatValidator {
    private static final RomanNumberFormatValidator romanValidator = new RomanNumberFormatValidator();

    /**
     * The regular expression used for parsing single intergalactic digits
     */
    public static final String DIGIT_REGEX = "\\b[A-Za-z0-9]+\\b";

    /**
     * The regular expression used for detecting intergalactic digits separator
     */
    public static final String DIGIT_SEPARATOR = " ";

    /**
     * The regular expression used for detecting intergalactic numbers composed
     * by one or more digits and separators
     */
    public static final String NUMBER_REGEX = "(" + DIGIT_REGEX + DIGIT_SEPARATOR + "{0,1})+";

    /**
     * Validates the format of a given String as intergalactic number. It takes in account the digits registered in
     * {@link com.machucarro.numerical.numbers.IntergalacticNumber#DIGITS}
     * @param intergalacticNumber
     * @return true if the given String represents a valid intergalactic number
     * @throws IntergalacticNumberFormatException
     */
    public boolean validate(String intergalacticNumber) throws IntergalacticNumberFormatException {
        if (intergalacticNumber == null || intergalacticNumber.isEmpty()) {
            throw new IntergalacticNumberFormatException(
                    "No intergalactic number specified. The value provided was null or empty");
        }
        if (!intergalacticNumber.matches(NUMBER_REGEX)) {
            throw new IntergalacticNumberFormatException(
                    "The specified value must contain only characters from A to "
                            + "Z, a to z, digits from 0 to 9 and \"" + DIGIT_SEPARATOR + "\"");
        }

        String romanValue = IntergalacticToRomanConverter.transliterateToRoman(intergalacticNumber);
        try {
            return romanValidator.validate(romanValue);
        } catch (RomanNumberFormatException e) {
            throw new IntergalacticNumberFormatException("Invalid intergalactic number "
                    + intergalacticNumber);
        }
    }

    /**
     * Validates a given {@link com.machucarro.numerical.numbers.IntergalacticNumber}
     * @param intergalacticNumber
     * @return true if it is a valid intergalactic number
     * @throws IntergalacticNumberFormatException
     */
    public boolean validate(IntergalacticNumber intergalacticNumber)
            throws IntergalacticNumberFormatException {
        return this.validate(intergalacticNumber.toString());
    }

    /**
     * @see com.machucarro.numerical.validators.NumberFormatValidator#validate(Number)
     * @param value
     * @param <T>
     * @return
     */
    @Override
    public <T extends Number> boolean validate(T value) {
        if (value instanceof IntergalacticNumber) {
            return this.validate((IntergalacticNumber)value);
        } else {
            throw new IntergalacticNumberFormatException(
                    "The number provided is not an IntergalacticNumber instance " + value);
        }
    }
}
