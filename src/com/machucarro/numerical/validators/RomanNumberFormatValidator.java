package com.machucarro.numerical.validators;

import com.machucarro.numerical.exceptions.RomanNumberFormatException;
import com.machucarro.numerical.numbers.RomanNumber;

/**
 * This class validates the correctness of roman numeric expressions and the
 * suitability of arabic numbers for its conversion to Roman system
 * 
 * @author MAchucarro
 */
public class RomanNumberFormatValidator implements NumberFormatValidator {

    /**
     * Regular expression for validating the combinations in the thousands.
     * There may be from 0 to 3 M in the thousands
     */
    public static final String THOUSANDS_REGEX = "M{0,3}";

    /**
     * Regular expression for validating the combinations in the hundreds:
     * <ul>
     *     <li> 9 hundreds are CM. Only C may precede M</li>
     *     <li> 5 to 8 hundreds begin with D followed by 0 to 3 C</li>
     *     <li> 4 hundreds are CD, Only C may precede D</li>
     *     <li> 3 and less will have 0 to 3 C</li>
     * </ul>
     */
    public static final String HUNDREDS_REGEX = "(CM|DC{0,3}|CD|C{0,3})";

    /**
     * The combinations in the tenths:
     * <ul>
     *     <li> 9 tenths are XC. Only X may precede C</li>
     *     <li> 5 to 8 tenths begin with L followed by 0 to 3 X</li>
     *     <li> 4 tenths are XL, Only X may precede L</li>
     *     <li> 3 and less will have 0 to 3 X</li>
     * </ul>
     */
    public static final String TENTHS_REGEX = "(XC|LX{0,3}|XL|X{0,3})";

    /**
     * Regular expression for validating the combinations in the units:
     * <ul>
     *     <li> 9 units are IX. Only I may precede X</li>
     *     <li> 5 to 8 units begin with V followed by 0 to 3 I</li>
     *     <li> 4 units are IV, Only I may precede V</li>
     *     <li> 3 and less will have 0 to 3 I</li>
     * </ul>
     */
    public static final String UNITS_REGEX = "(IX|VI{0,3}|IV|I{0,3})";
    /**
     * Regular expression used to validate any roman number according to the
     * rules defined in the exercise
     */
    public static final String NUMBER_REGEX = "^" + THOUSANDS_REGEX + HUNDREDS_REGEX + TENTHS_REGEX
            + UNITS_REGEX + "$";
    public static final String DIGIT_REGEX = "[IVXLCDM]{1}";

    /**
     * Validates a roman number expressed as a String
     * 
     * @param romanNumber
     *            the roman number to be validated
     * @return true if romanNumber meets with the roman format conventions
     * @throws com.machucarro.numerical.exceptions.RomanNumberFormatException
     *             if the specified romanNumber does not meet the roman format
     *             conventions
     */
    public boolean validate(String romanNumber) throws RomanNumberFormatException {
        if (romanNumber == null || romanNumber.isEmpty()) {
            throw new RomanNumberFormatException(
                    "No roman number specified. The value provided was null or empty");
        } else if (!romanNumber.matches(NUMBER_REGEX)) {
            throw new RomanNumberFormatException("Invalid roman number " + romanNumber);
        }
        return true;
    }

    /**
     * Validates a roman number
     *
     * @param romanNumber
     *            the roman number to be validated
     * @return true if romanNumber meets with the roman format conventions
     * @throws com.machucarro.numerical.exceptions.RomanNumberFormatException
     *             if the specified romanNumber does not meet the roman format
     *             conventions
     */
    public boolean validate(RomanNumber romanNumber) {
        return this.validate(romanNumber.toString());
    }

    /**
     * @see com.machucarro.numerical.validators.NumberFormatValidator#validate(Number)
     * @param value
     * @param <T>
     * @return
     */
    @Override
    public <T extends Number> boolean validate(T value) {
        if (value instanceof RomanNumber) {
            return this.validate((RomanNumber)value);
        } else {
            throw new RomanNumberFormatException(
                    "The number provided is not a RomanNumber instance " + value);
        }
    }
}
