package com.machucarro.numerical.validators;

import com.machucarro.numerical.exceptions.RomanNumberFormatException;

/**
 * 
 * This class validates integers for its conversion into roman numbers
 * 
 * @author MAchucarro
 */
public class IntegerToRomanConversionValidator implements NumberFormatValidator {
    /**
     * This is the maximal integer value which could be represented in roman
     * numbers according to the rules defined in the exercise
     */
    public static final int MAX_INT_VALUE = 3888;
    /**
     * The minimum value possible to represent in roman numbers without zero is
     * 1
     */
    public static final int MIN_INT_VALUE = 1;

    /**
     * Validates an int number for its conversion to roman
     * 
     * @param integerNumber
     *            the int number to be converted
     * @return true if integerNumber can be converted to roman
     * @throws com.machucarro.numerical.exceptions.RomanNumberFormatException
     *             if the specified integerNumber cannot be converted into roman
     */
    public boolean validate(Integer integerNumber) throws RomanNumberFormatException {
        if (integerNumber == null) {
            throw new RomanNumberFormatException(
                    "No integer number specified. The value provided was null");
        } else if (integerNumber < MIN_INT_VALUE) {
            throw new RomanNumberFormatException("Invalid integer number specified "
                    + integerNumber + ". No number " + "smaller than " + MIN_INT_VALUE
                    + " has a roman equivalent according to the specified rules");
        } else if (integerNumber > MAX_INT_VALUE) {
            throw new RomanNumberFormatException("Invalid integer number specified "
                    + integerNumber + ". No number " + "greater than " + MAX_INT_VALUE
                    + " has a roman equivalent according to the specified rules");
        }
        return true;
    }

    /**
     * @see com.machucarro.numerical.validators.NumberFormatValidator#validate(Number)
     * @param value
     * @param <T>
     * @return
     */
    @Override
    public <T extends Number> boolean validate(T value) {
        if (value instanceof Integer) {
            return this.validate((Integer)value);
        } else {
            throw new RomanNumberFormatException("The number provided is not an Integer instance "
                    + value);
        }
    }
}
