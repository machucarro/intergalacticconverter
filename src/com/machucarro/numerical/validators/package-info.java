/**
 * This package contains the Validator classes used for validating the formats of the numbers contained in {@link com
 * .machucarro.numerical.numbers} and its conversions handled by the classes in {@link com.machucarro.numerical.converters}
 *
 * @see com.machucarro.numerical.converters
 * @see com.machucarro.numerical.numbers
 * @see com.machucarro.numerical
 * @author MAchucarro
 */
package com.machucarro.numerical.validators;