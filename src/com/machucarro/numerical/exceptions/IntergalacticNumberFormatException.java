package com.machucarro.numerical.exceptions;

/**
 * This Exception is used for validation errors of intergalactic numbers
 * 
 * @author MAchucarro
 */
public class IntergalacticNumberFormatException extends NumberFormatException {
    public IntergalacticNumberFormatException(String message) {
        super(message);
    }
}
