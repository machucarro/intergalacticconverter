package com.machucarro.numerical.exceptions;

/**
 * 
 * This Exception is used for validation errors of roman numbers
 * 
 * @author MAchucarro
 */
public class RomanNumberFormatException extends NumberFormatException {
    public RomanNumberFormatException(String message) {
        super(message);
    }
}
